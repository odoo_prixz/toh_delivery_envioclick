# -*- coding: utf-8 -*-
{
    'name': "Método de envío por Envíoclick",

    'summary': """
        Shipping method by Envioclick provider.
        Proveedor Envíoclick para método de envío.""",

    'description': """
        Uso de la API de Envíoclick en su versión 1.0 (https://api.envioclickpro.com/)
    """,

    'author': "Toh Soluciones Digitales",
    'website': "http://www.tohsoluciones.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Operations/Inventory/Delivery',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['delivery', 'mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/provider_envioclick_view.xml',
        'views/sale_order_views.xml',
        'wizard/choose_delivery_carrier_views.xml',
        'data/delivery_envioclick_data.xml',
    ],
    'price': '275.00',
    'currency': 'USD',
    'installable': True,
    'application': True,
    'images': ["static/description/seleccion_envioclick_ventas.jpg"],
    'license': 'OPL-1',
}
