# -*- coding: utf-8 -*-
import json
import logging
from .envioclick_request import EnvioclickRequest
from odoo import models, fields, api, _
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

class ProviderEnvioclick(models.Model):
    _inherit = 'delivery.carrier'

    #ondelete para V14
    #delivery_type = fields.Selection(selection_add=[('envioclick', "Envíoclick")])
    delivery_type = fields.Selection(selection_add=[('envioclick', "Envíoclick")], 
        ondelete={'envioclick': lambda recs: recs.write({'delivery_type': 'fixed', 'fixed_price': 0})})
    envioclick_api_key = fields.Char(string="API Key Authentication")
    envioclick_default_packaging_id = fields.Many2one('product.packaging', string='Envíoclick Default Packaging Type')
    envioclick_request_pickup = fields.Boolean('Requiere recoger')
    envioclick_insurance = fields.Boolean('Seguro')
    envioclick_thermal_label = fields.Boolean('Requiere impresion térmica')

# =====================================================================
#   _COMPUTE_CAN_GENERATE_RETURN(SELF)
# =====================================================================
    def _compute_can_generate_return(self):
        super(ProviderEnvioclick, self)._compute_can_generate_return()
        for carrier in self:
            if carrier.delivery_type == 'envioclick':
                carrier.can_generate_return = True

    # =====================================================================
    #   fill_carries_lines(SELF, RATES)
    # =====================================================================
    def fill_carries_lines(self, rates):
        vals_carrier_line = []
        for record in rates:
            print("hola")
            vals = {
                'idRate': record.get('idRate'),
                'type': record.get('product'),
                'carrier': record.get('carrier'),
                'price_total': record.get('total'),
                'deliveryDays': record.get('deliveryDays'),
                'quotationType': record.get('quotationType'),
            }
            vals_carrier_line.append((0, 0, vals))
        return vals_carrier_line

# =====================================================================
#   ENVIOCLICK_RATE_SHIPMENT(SELF, ORDER)
# =====================================================================
    def envioclick_rate_shipment(self, order):
        ''' Compute the price of the order shipment

        :param order: record of sale.order
        :return dict: {'success': boolean,
                       'price': a float,
                       'error_message': a string containing an error message,
                       'warning_message': a string containing a warning message}
                       # TODO maybe the currency code?
        '''
        envio_desde = order.warehouse_id.partner_id
        envio_hasta = order.partner_shipping_id
#        warehouse_partner_id = order.warehouse_id.partner_id
        #currency_id = order.currency_id or order.company_id.currency_id
        #total_value = sum([line.product_id.lst_price * line.product_qty for line in order.order_line if not line.display_type])
#        destination_partner_id = order.partner_id
        
        envioclickrequest = EnvioclickRequest(self.log_xml, prod_environment=self.prod_environment)
        error_check_value = envioclickrequest.check_required_value(self, envio_hasta, envio_desde, order=order)
        
        #Si la revisión de los valores no es correcta devolverá True el campo 'check_value'
        if error_check_value:
            return {'success': False,
                    'price': 0.0,
                    'error_message': error_check_value,
                    'warning_message': False}
        
        response = envioclickrequest.get_cotizacion(self, order)        
        
        if response.status_code == 200:
            _return = envioclickrequest.compute_price(response)
            json_response = json.loads(response.text)
            vals_carrier_line = self.fill_carries_lines(json_response['data'].get('rates'))
            if _return:
                _return['return']['vals_carrier_line'] = vals_carrier_line
                return _return['return']
            else:
                return {'success': False,
                        'price': 0.0,
                        'error_message': 'Ningún proveedor encontrado',
                        'warning_message': False}
        elif response.status_code == 401:
            return {'success': False,
                    'price': 0.0,
                    'error_message': 'Unauthorized.',
                    'warning_message': False}
        elif response.status_code == 403:
            return {'success': False,
                    'price': 0.0,
                    'error_message': 'Acceso denegado.',
                    'warning_message': False}
        elif response.status_code == 422:
            error_msg = False
            json_r = response.json()
#            response_json = response.text.encode('utf8')
            error = json_r['status_messages']
            error_msg = 'Error 422 (Unprocessed Entity) - ' + str(error['error'])
            return {'success': False,
                    'price': 0.0,
                    'error_message': error_msg,
                    'warning_message': False}
#            return {'success': False,
#                    'price': 0.0,
#                    'error_message': 'Unprocessed Entity.',
#                    'warning_message': False}            
        elif response.status_code == 499:
            return {'success': False,
                    'price': 0.0,
                    'error_message': 'Unknown error.',
                    'warning_message': False}
        else:
            return {'success': False,
                    'price': 0.0,
                    'error_message': 'Error desconocido.',
                    'warning_message': False}
        
        
# =====================================================================
#   ENVIOCLICK_SEND_SHIPPING(SELF, PICKINGS)
# =====================================================================    
    def envioclick_send_shipping(self, pickings):
        ''' Send the package to the service provider

        :param pickings: A recordset of pickings
        :return list: A list of dictionaries (one per picking) containing of the form::
                         { 'exact_price': price,
                           'tracking_number': number }
                           # TODO missing labels per package
                           # TODO missing currency
                           # TODO missing success, error, warnings
        '''
        
        res = []
        envioclickrequest = EnvioclickRequest(self.log_xml, prod_environment=self.prod_environment)

#       ------------ Recorre las  ordenes de entrega ------------ 
        for picking in pickings:
            #Obtiene el partner_id con la ruta Orden de entrega/Tipo de operación/Almacén/
            envio_desde = picking.picking_type_id.warehouse_id.partner_id 
            #Obtiene el partner_id de la orden de entrega
            envio_hasta = picking.partner_id 
            order = picking.sale_id
            
            error_check_value = envioclickrequest.check_required_value(self, envio_hasta, envio_desde, order=order)
        
            #Si la revisión de los valores no es correcta devolverá True
            if error_check_value:
                raise UserError(error_check_value)
            
#       ------------ Obtener precio ------------             
            response = envioclickrequest.get_cotizacion(self, order)        
        
            if response.status_code == 200:
                if picking.sale_id and picking.sale_id.is_delivery:
                    _return = envioclickrequest.compute_price(response, order_id=picking.sale_id)
                else:
                    _return = envioclickrequest.compute_price(response)
                if _return:
                    rate = _return['rate']
                    return_precio = _return['return']
                    precio = return_precio['price']
                    json_package = envioclickrequest.get_json_package(self, order)
                    enviar_response = envioclickrequest.enviar(self, rate, json_package, order, picking)
                    if enviar_response.status_code == 200:
                        enviar_response_json = enviar_response.json()
                        data_json = enviar_response_json['data']
                        tracker_number = data_json['tracker']
                        envioclick_guia = data_json['guide']
                        envioclick_url = data_json['url']
                        #envioclick_id_order = data_json['idOrder']
                        shipping_data = {'exact_price': precio,
                                         'tracking_number': tracker_number}
                        res = res + [shipping_data]
                        picking.write({'envioclick_guia': envioclick_guia,
                                       'envioclick_url': envioclick_url})
                    elif enviar_response.status_code == 401:
                        raise UserError('Unauthorized.')                        
                    elif enviar_response.status_code == 403:
                        raise UserError('Acceso denegado.')                        
                    elif enviar_response.status_code == 422:
                        error_msg = False
                        json_r = enviar_response.json()
                        error = json_r['status_messages']
                        try:
                            error_msg = 'Error 422 (Unprocessed Entity) - ' + str(error['error'])
                        except Exception as e:
                            _logger.error("*******ERROR OCURRIDO AL PROCESAR ERROR DE RESPONSE CON STATUS_CODE 422*******")
                            _logger.error(e)
                            error_msg = 'Error 422 (Unprocessed Entity) - ' + json_r['error']
                        raise UserError(error_msg)                        
                    elif enviar_response.status_code == 499:
                        raise UserError('Unknown error.')                        
                    else:
                        raise UserError('Error desconocido.')
                else:
                    raise UserError('Ningún proveedor encontrado')
            elif response.status_code == 401:
                raise UserError('Unauthorized.')                        
            elif response.status_code == 403:
                raise UserError('Acceso denegado.')                        
            elif response.status_code == 422:
                error_msg = False
                json_r = response.json()
                error = json_r['status_messages']
                try:
                    error_msg = 'Error 422 (Unprocessed Entity) - ' + str(error['error'])
                except Exception as e:
                    _logger.error("*******ERROR OCURRIDO AL PROCESAR ERROR DE RESPONSE CON STATUS_CODE 422*******")
                    _logger.error(e)
                    error_msg = 'Error 422 (Unprocessed Entity) - ' + json_r['error']
                raise UserError(error_msg)                        
            elif response.status_code == 499:
                raise UserError('Unknown error.')                        
            else:
                raise UserError('Error desconocido.')
                    
#        for picking in pickings:
#            shipping_data = {'exact_price': 100.49,
#                             'tracking_number': 104593}
#            res = res + [shipping_data]
        return res

# =====================================================================
#   ENVIOCLICK_GET_TRACKING_LINK(SELF, PICKING)
# =====================================================================    
    def envioclick_get_tracking_link(self, picking):
        ''' Ask the tracking link to the service provider

        :param picking: record of stock.picking
        :return str: an URL containing the tracking link or False
        '''
        return picking.envioclick_url #'http://tohsoluciones.com/search?itemCode=%s&lang=en' % picking.carrier_tracking_ref

# =====================================================================
#   ENVIOCLICK_CANCEL_SHIPMENT(SELF, PICKINGS)
# =====================================================================
    def envioclick_cancel_shipment(self, pickings):
        ''' Cancel a shipment

        :param pickings: A recordset of pickings
        '''
        for picking in pickings:
            # Obviously you need a pick up date to delete SHIPMENT by DHL. So you can't do it if you didn't schedule a pick-up.
            picking.message_post(body=_(u"You can't cancel Envíoclick shipping without pickup date."))
            picking.write({'carrier_tracking_ref': '',
                           'carrier_price': 0.0})

# =====================================================================
#   _GET_DEFAULT_CUSTOM_PACKAGE_CODE(SELF)
# =====================================================================
    def _get_default_custom_package_code(self):
        """ Some delivery carriers require a prefix to be sent in order to use custom
        packages (ie not official ones). This optional method will return it as a string.
        """
        
# =====================================================================
#   _ENVIOCLICK_CONVERT_WEIGHT(SELF, WEIGHT)
#   El peso en Envíoclick se tiene que representar en Kilogramos
# =====================================================================
    def _envioclick_convert_weight(self, weight):
        weight_uom_id = self.env['product.template']._get_weight_uom_id_from_ir_config_parameter()
        return weight_uom_id._compute_quantity(weight, self.env.ref('uom.product_uom_kgm'), round=False)