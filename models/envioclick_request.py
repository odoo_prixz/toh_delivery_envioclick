import logging
import requests
import json
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

class EnvioclickRequest():
    
    def __init__(self, debug_logger, prod_environment=False):
        self.debug_logger = debug_logger
        #if not prod_environment:
        self.prod_environment = prod_environment
        self.url = 'https://api.envioclickpro.com/api/v1'
   

    def check_required_value(self, carrier, envio_hasta, envio_desde, order=False):
        carrier = carrier.sudo()
        recipient_required_field = ['city', 'zip', 'country_id']
        
#       ------------ Verifica que el campo envioclick_api_key esté configurado ------------        
        if not carrier.envioclick_api_key:
            return ("Campo API Key Authentication de Envíoclick no encontrado, por favor modifica la configuración de tu método de envío.")
        #SE QUITA EL CAMPPO REQUERIDO
        #if not envio_hasta.phone and not envio_hasta.mobile:
        #    recipient_required_field.append('phone')
        #    recipient_required_field.append('mobile')
            
        #if not envio_hasta.email:
        #    if not envio_hasta.parent_id or not envio_hasta.parent_id.email:
        #        recipient_required_field.append('email')
#       ------------ Verificar que street y street2 estén configurados ------------            
#       ------------ Por módulo l10n_mx_edi y base_address_extended se contempla nombre de calle, número y colonia ------------
        if not envio_hasta.street: #street and not envio_hasta.street2:
            recipient_required_field.append('street')
        #if not envio_hasta.street_number:
        #     recipient_required_field.append('street_number')
        """if not envio_hasta.street2:
            recipient_required_field.append('street2')
            #recipient_required_field.append('l10n_mx_edi_colony')"""
        
        res = [field for field in recipient_required_field if not envio_hasta[field]]
        if res:
            return ("Falta la dirección del cliente o es incorrecta (Campo faltante(s) :\n %s)") % ", ".join(res).replace("_id", "")

        shipper_required_field = ['city', 'zip', 'phone', 'country_id']
        if not envio_desde.street2: #street and not envio_desde.street2:
            shipper_required_field.append('street')

        res = [field for field in shipper_required_field if not envio_desde[field]]
        if res:
            return ("Falta la dirección del almacén de su empresa o es incorrecta (Campo faltante(s) :\n %s)") % ", ".join(res).replace("_id", "")
        
#       ------------ VERIFICA PAISES CONFIGURADOS ------------
        codigo_paises = [
                country.code for country in carrier.country_ids if country.code]
        _logger.info('*******CODIGO DE PAISES %s ', codigo_paises)
        
        if codigo_paises:
            if envio_hasta.country_id.code not in codigo_paises:
                return ("Es posible que el país " + envio_hasta.country_id.code + " de destino no se encuentre en el alcance.")
            if envio_desde.country_id.code not in codigo_paises:
                return ("Es posible que el país " + envio_desde.country_id.code + " de origen no se encuentre en el alcance.")
#            return ",".join(codigo_paises)
        
#       ------------ Verificar valores dentro la orden ------------ 
        if order:
            #Verifica que existan productos diferentes a 'service' y 'digital'
            if order.order_line and all(order.order_line.mapped(lambda l: l.product_id.type in ['service', 'digital'])):
                return ("The estimated shipping price cannot be computed because all your products are service/digital.")
            #Verifica que contenga productos
            if not order.order_line:
                return ("Please provide at least one item to ship.")
            #Verifica que todos los productos seleccionados tengan configurado el campo de peso 'weight'
            #Dejamos el peso fijo como se esta mandando actualemnte en prixz

            #if order.order_line.filtered(lambda line: not line.product_id.weight and not line.is_delivery and line.product_id.type not in ['service', 'digital'] and not line.display_type):
            #    return ('The estimated shipping cannot be computed because the weight of your product is missing.')
            
#       ------------ El peso no debe ser mayor a los 70 KG ------------             
            #weight = sum([(line.product_id.weight * line.product_qty) for line in order.order_line if not line.display_type]) or 0.0
            weight = 1
            weight_in_kg = carrier._envioclick_convert_weight(weight)
            
            if weight_in_kg > 70:
                return ('No se permiten envíos mayores a 70 Kg.')

        #Devuelve Falso en caso de que todas las validaciones estén correctas
        return False
    

# =====================================================================
#   GET_COTIZACION(SELF, CARRIER, ORDER)
# ===================================================================== 
    def get_cotizacion(self, carrier=False, order=False):
        carrier = carrier.sudo()
        api_key = carrier.envioclick_api_key
        error = {}
        
        headers = {
                    'Content-Type': 'application/json',
                    'Authorization': api_key #'1fbe55f3-f7e9-472b-95ed-3402246251fe',
                  }
        
        
#        weight = sum([(line.product_id.weight * line.product_qty) for line in order.order_line if not line.display_type]) or 0.0
#        weight_in_kg = carrier._envioclick_convert_weight(weight)

#        envio_desde = order.warehouse_id.partner_id
#        envio_hasta = order.partner_shipping_id
        
#        packaging = carrier.envioclick_default_packaging_id
        
#        payload = {
#                    "package": {
#                        "description": "Pink iPad",
#                        "contentValue": 120.01,
#                        "weight": weight_in_kg,
#                        "length": packaging.length, #30.01,
#                        "height": packaging.height, #15.01,
#                        "width": packaging.width #20.01
#                    },
#                    "origin_zip_code": envio_desde.zip,
#                    "destination_zip_code": envio_hasta.zip
#                }
        
        payload = self.get_json_package(carrier, order)
        
        url_quotation = self.url + '/quotation_multipackages' #'/quotation'
        
        response = requests.post(url_quotation, headers=headers, json = payload)
                
        #response.text.encode('utf8')
        _logger.info('*******URL %s ', url_quotation)
        _logger.info('*******HEADERS %s ', headers)
        _logger.info('*******DATA %s ', payload)
        _logger.info('*******RESPONSE TEXT %s ', response.text.encode('utf8'))
        return response

# =====================================================================
#   COMPUTE_PRICE(SELF, RESPONSE)
# ===================================================================== 
    def compute_price(self, response, order_id=False):
        json_r = response.json()
        json_data = json_r['data']
        
        total_anterior = 0
        rate = {}
        if order_id:
            for rates in json_data['rates']:
                if rates['idRate'] == order_id.idRate or (
                        rates['total'] == order_id.price_total and rates['deliveryDays'] == order_id.deliveryDays):
                    rate = rates
                    break
        if not rate:
            for rates in json_data['rates']:
                print('Carrier: ' + rates['carrier'])
                total_actual = rates['total']
                if total_actual < total_anterior or total_anterior == 0:
                    total_anterior = total_actual
                    rate = rates
        
        _logger.info('*******RATE %s ', rate)
        _logger.info('*******TOTAL_ANTERIOR %s ', total_anterior)
        
        resp = {}
        
        if rate:
            resp = {
                    'rate': rate,
                    'return': {
                            'success': True,
                            'price': rate['total'],
                            'error_message': False,
                            'warning_message': False
                        }
                    }
        else:
            resp = False
            
        return resp
    
    
# =====================================================================
#   ENVIAR(SELF, RESPONSE)
# ===================================================================== 
    def enviar(self, carrier=False, rate=False, json_package=False, sale_order=False, picking=False):
        carrier = carrier.sudo()
        api_key = carrier.envioclick_api_key
        
        url_envio = False
        if self.prod_environment:
            url_envio = self.url + '/shipmentMultiPackages/request'
        else:
            url_envio = self.url + '/sandbox_shipmentMultiPackages/request' #'/sandbox_shipment/request'
        
        if not rate:
            raise UserError("error_check_value")
        
        envio_desde = picking.picking_type_id.warehouse_id.partner_id if picking.picking_type_id.warehouse_id else sale_order.warehouse_id.partner_id
        envio_hasta = sale_order.partner_shipping_id
        full_address_destino = ""
        if envio_hasta.street:
            full_address_destino = envio_hasta.street
        if envio_hasta.street2:
            full_address_destino += envio_hasta.street
        
        _logger.info('*******FULL_ADDRESS_DESTINO %s ', full_address_destino)
        
        phone_number_desde = 'N/A'
        if envio_desde.phone:
            phone_number_desde = envio_desde.phone
        elif envio_desde.mobile:
            phone_number_desde = envio_desde.mobile
        
        
        phone_number_hasta = phone_number_desde
        if envio_hasta.phone:
            phone_number_hasta = envio_hasta.phone
        elif envio_hasta.mobile:
            phone_number_hasta = envio_hasta.mobile
        
        correo_hasta = envio_hasta.email
        if not correo_hasta:
            correo_hasta = envio_desde.email
        
        colonia_desde = 'N/A'
        if envio_desde.street2:
            colonia_desde = envio_desde.street2
        
        headers = {
                    'Content-Type': 'application/json',
                    'Authorization': api_key #'1fbe55f3-f7e9-472b-95ed-3402246251fe',
                  }
        
        payload = {
            "idRate": rate['idRate'],
            "myShipmentReference": picking.sale_id.name,
            "requestPickup": carrier.envioclick_request_pickup,
            "pickupDate": "2017-12-20",
            "insurance": carrier.envioclick_insurance,
            "thermalLabel": carrier.envioclick_thermal_label,
            "packages": json_package['packages'],
            "origin": {
                "company": envio_desde.name[0:28],
                "firstName": envio_desde.name[0:14],
                "lastName": "N/A",
                "email": envio_desde.email[0:60] if envio_desde.email else "N/A",
                "phone": phone_number_desde[0:10], #envio_desde.phone[0:10],
                "street": envio_desde.street[0:29],
                "number": "N/A",#envio_desde.street_number[0:5], #envio_desde.street2,
                "suburb": colonia_desde[0:30], #envio_desde.l10n_mx_edi_colony, #"My suburb (colonia)",
                "crossStreet": "N/A", #"Street1 and street2",
                "reference": "N/A", #"Big white window",
                "zipCode": envio_desde.zip[0:5] #json_package['origin_zip_code']
            },
            "destination": {
                "company": envio_hasta.name[0:28],
                "firstName": envio_hasta.name[0:14],
                "lastName": "N/A",
                "fullName": "N/A",
                "email": correo_hasta[0:60], #envio_hasta.email[0:60],
                "phone": phone_number_hasta[0:10], #envio_hasta.phone[0:10],
                "street": envio_hasta.street[0:29],
                "number": "",#envio_hasta.street_number[0:5],
                "suburb": envio_hasta.street2[0:30] if envio_hasta.street2 else "N/A", #"N/A",
                "fullAddress": full_address_destino[0:200],
                "state": envio_hasta.state_id.display_name[0:50],
                "crossStreet": "N/A",
                "reference": "N/A",
                "zipCode": envio_hasta.zip[0:5] #json_package['destination_zip_code']
            }
        }
        
        _logger.info('*******URL %s ', url_envio)
        _logger.info('*******HEADERS %s ', headers)
        _logger.info('*******DATA %s ', payload)
        
        response = requests.post(url_envio, headers=headers, json = payload)
                
        #response.text.encode('utf8')
        _logger.info('*******RESPONSE TEXT %s ', response.text.encode('utf8'))
        _logger.info('*******STATUS_CODE %s ', response.status_code)
        
        return response
    
# =====================================================================
#   GET_JSON_PACKAGE(SELF, CARRIER, SALE_ORDER)
# ===================================================================== 
    def get_json_package(self, carrier=False, sale_order=False):
        carrier = carrier.sudo()
        
        envio_desde = sale_order.warehouse_id.partner_id
        envio_hasta = sale_order.partner_shipping_id
        
        packaging = carrier.envioclick_default_packaging_id
        
        json_package = {}
        json_package['packages'] = []
        
        for order_line in sale_order.order_line:
            if not order_line.display_type and order_line.product_id.type not in ['service', 'digital']:
                peso = order_line.product_id.weight * order_line.product_qty
                peso_en_kg = carrier._envioclick_convert_weight(peso)
        
                json_package['packages'].append({
                    "description": "Medicamento",
                    "contentValue": order_line.price_total,
                    #Se pone estatico los pesos y tamaños para PRIXZ
                    "weight": 1.0,#peso_en_kg,
                    "length": 20.0,#packaging.packaging_length, #packaging.length, #En V13 el campo es length
                    "height": 20.0,#packaging.height, #15.01,
                    "width": 20.0,#packaging.width #20.01
                })
                break
        payload = {
            "packages": json_package['packages'],
            "origin_zip_code": envio_desde.zip,
            "destination_zip_code": envio_hasta.zip
        }
                
        return payload
        
#        weight = sum([(line.product_id.weight * line.product_qty) for line in order.order_line if not line.display_type]) or 0.0
#        weight_in_kg = carrier._envioclick_convert_weight(weight)

#        envio_desde = order.warehouse_id.partner_id
#        envio_hasta = order.partner_shipping_id
        
#        packaging = carrier.envioclick_default_packaging_id
        
#        payload = {
#                    "package": {
#                        "description": "Pink iPad",
#                        "contentValue": 120.01,
#                        "weight": weight_in_kg,
#                        "length": packaging.length, #30.01,
#                        "height": packaging.height, #15.01,
#                        "width": packaging.width #20.01
#                    },
#                    "origin_zip_code": envio_desde.zip,
#                    "destination_zip_code": envio_hasta.zip
#                }