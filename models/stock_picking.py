from datetime import datetime

from odoo import models, fields, api
from odoo.exceptions import UserError
import logging
import requests

_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
    _inherit = 'stock.picking'
    
    
    envioclick_guia = fields.Char(string="Guía Envíoclick")
    envioclick_url = fields.Char(string="URL Envíoclick")

    # =====================================================================
    #   GET_tracking_state(SELF, num_tracking)
    # =====================================================================
    def get_cotizacion(self, carrier=False, tracking_code=False):
        carrier = carrier.sudo()
        api_key = carrier.envioclick_api_key
        vals_state = {}
        headers = {
            'Content-Type': 'application/json',
            'Authorization': api_key  # '1fbe55f3-f7e9-472b-95ed-3402246251fe',
        }

        payload = self.get_json_totracking_code(tracking_code)

        url_quotation = 'https://api.envioclickpro.com/api/v2/track'

        response = requests.post(url_quotation, headers=headers, json=payload)
        json_response = response.json()
        # response.text.encode('utf8')
        if response.ok:
            error = json_response.get("data").get('error')
            if not error:
                vals_state = self.get_date_and_status_envio_click_response(json_response)
        return vals_state

    def get_json_totracking_code(self, tracking_code):
        return {
            "trackingCode": tracking_code
        }

    def get_date_and_status_envio_click_response(self, json_response):
        data = json_response.get("data")
        vals_state = {
            "state": data.get("status"),
            "date": datetime.fromisoformat(data.get('arrivalDate'))
        }
        return vals_state
