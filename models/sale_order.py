from odoo import models, fields, api


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    idRate = fields.Integer(string="Id rate", )
    carrier = fields.Char(string="Paqueteria", )
    price_total = fields.Float(string="Precio", )
    deliveryDays = fields.Integer(string="Días de entrega", )
    is_delivery = fields.Boolean(string="Contiene paqueteria", )
