# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError

class ChooseDeliveryCarrier(models.TransientModel):
    _inherit = 'choose.delivery.carrier'

    carrier_ids = fields.One2many(
        comodel_name='choose.delivery.carrier.lines',
        inverse_name='chose_carrier_id',
        string="Routing Lines"
    )

    def _get_shipment_rate(self):
        res = super(ChooseDeliveryCarrier, self)._get_shipment_rate()
        vals = self.carrier_id.rate_shipment(self.order_id)
        if vals.get('vals_carrier_line'):
            self.carrier_ids = vals.get('vals_carrier_line')
            #Se ponen en default en cero
            self.delivery_price = 0
            self.display_price = 0
        return res

    def button_confirm(self):
        carrier_id = self.carrier_ids.filtered(lambda x: x.is_selected == True)
        if len(carrier_id) > 1:
            raise ValidationError(f'Solo se puede seleccionar una paquetería')
        if carrier_id:
            self.order_id.idRate = carrier_id.idRate
            self.order_id.carrier = carrier_id.carrier
            self.order_id.price_total = carrier_id.price_total
            self.order_id.deliveryDays = carrier_id.deliveryDays
            self.order_id.is_delivery = True
        else:
            raise ValidationError(f'Debe seleccionar una paquetería')
        res = super(ChooseDeliveryCarrier, self).button_confirm()
        return res


class ChooseDeliveryCarrierLine(models.TransientModel):
    _name = 'choose.delivery.carrier.lines'
    _description = 'Paqueterias disponibles'

    chose_carrier_id = fields.Many2one(comodel_name='choose.delivery.carrier', string = 'Carrier', )
    idRate = fields.Integer(string="Id rate", )
    type = fields.Char(string="Tipo", )
    carrier = fields.Char(string="Paqueteria", )
    price_total = fields.Float(string="Precio", )
    deliveryDays = fields.Integer(string="Días de entrega", )
    quotationType = fields.Char(string="Tipo de cotizacion", )
    is_selected = fields.Boolean(string="¿Seleccionado?", default=False)

    def seleccionar_paqueteria(self):
        self.chose_carrier_id.carrier_ids.is_selected = False
        self.is_selected = True
